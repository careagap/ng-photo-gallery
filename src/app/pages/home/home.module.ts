import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { MatDialogModule } from '@angular/material/dialog';
import { ImageDetailsComponent } from './components/image-details/image-details.component';


@NgModule({
  declarations: [HomeComponent, ImageDetailsComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatDialogModule,
  ]
})
export class HomeModule { }
