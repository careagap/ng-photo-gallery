import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthToken } from '../models/auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
  ) { }

  getAccessToken(): Observable<AuthToken> {
    return <any>this.http.post(environment.auth.tokenUrl, { apiKey: environment.auth.apiKey });
  }
}
