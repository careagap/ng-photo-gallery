import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ImageDetails } from '../models/image-details';
import { Images } from '../models/images';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  private authToken = 'c3260b41780f08f55825ad879864a4a47a771ef7';

  constructor(
    private http: HttpClient,
  ) { }

  getImages(page: number = 1): Observable<Images> {
    return <any>this.http.get(environment.api_endpoints.images, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
      params: {
        page: page.toString(),
      }
    });
  }

  getImage(id: string): Observable<ImageDetails> {
    return <any>this.http.get(`${environment.api_endpoints.images}/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
  }
}
