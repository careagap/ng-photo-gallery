export interface AuthToken {
    auth: boolean;
    token: string;
}
