export interface Images {
    pictures: { id: string, cropped_picture: string }[];
    page: number;
    pageCount: number;
    hasMore: boolean;
}
