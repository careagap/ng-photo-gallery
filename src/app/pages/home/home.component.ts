import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ImageDetailsComponent } from './components/image-details/image-details.component';
import { Images } from './models/images';
import { ImagesService } from './services/images.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  images$: Observable<Images>;

  constructor(
    private imagesService: ImagesService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.images$ = this.imagesService.getImages();
  }

  openDialog(id: string) {
    console.log('openDialog id', id);
    const dialogRef = this.dialog.open(ImageDetailsComponent, {
      data: this.imagesService.getImage(id),
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
